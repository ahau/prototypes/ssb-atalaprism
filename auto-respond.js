/* eslint brace-style: ["error", "stroustrup"] */
const pull = require('pull-stream')
const { promisify: p } = require('util')
const debug = require('debug')('ssb-atala-prism:auto-respond')

const Issuer = require('./issuer')
const Verifier = require('./verifier')
const autoIssuanceSpec = require('./spec/auto-issuance')
const autoPresentationSpec = require('./spec/auto-presentation')

const {
  CRED_CONN_INVITE_SENT,
  CRED_CONN_COMPLETE,
  CRED_OFFER_SENT,
  CRED_COMPLETE
} = autoIssuanceSpec.states

const {
  PRES_CONN_INVITE_SENT,
  PRES_CONN_COMPLETE,
  PRES_REQUEST_SENT,
  PRES_SENT,
  PRES_COMPLETE
} = autoPresentationSpec.states

function handleError (err) {
  console.error(err)
}

module.exports = function autoRespond (ssb, config, crut) {
  pull(
    recordStream(ssb, crut.issuance),
    pull.drain(
      record => handleIssuance(ssb, config, crut.issuance, record),
      handleError
    )
  )

  pull(
    recordStream(ssb, crut.presentation),
    pull.drain(
      record => handlePresentation(ssb, config, crut.presentation, record),
      handleError
    )
  )
}

function recordStream (ssb, crut) {
  const type = crut.spec.type

  return pull(
    ssb.messagesByType({
      type,
      private: true,
      live: true,
      old: true
      // TODO crash recovery
    }),
    pull.filter(m => !m.sync),
    pull.map(getRoot),
    pull.asyncMap((key, cb) => {
      crut.read(key, (err, record) => {
        if (err) {
          debug(`${ssb.name || ssb.id} to read ${type} record: ${err.message}`)
          return cb(null, null)
        }
        cb(null, record)
      })
    }),
    pull.filter(Boolean)
  )
}

async function handleIssuance (ssb, config, crut, record) {
  if (record.state === CRED_CONN_INVITE_SENT) {
    if (!isRecipient(record, ssb.id)) return

    await ssb.atalaPrism.acceptConnectionInvitation(record.connection.oob)
      .then(() => {
        debug(CRED_CONN_COMPLETE)
        return crut.update(record.key, { state: CRED_CONN_COMPLETE })
      })
      .catch(handleError)
  }

  else if (record.state === CRED_CONN_COMPLETE) {
    if (!isInitiator(record, ssb.id)) return
    // this version means only the welcoming kaitiaki can complete the process

    const issuerConfig = config.atalaPrism?.issuers?.[record.tribeId]
    if (!issuerConfig) {
      console.log('No issuer config found for', record.tribeId)
      return
    }

    const issuer = new Issuer(issuerConfig)
    const DID = await issuer.getPrismDID()
      .catch(err => console.error('unable to get get prismDID', err))
    if (!DID) return

    // HACK we have to try multiple times for this to work sometimes
    retryUntil(
      () => issuer.issueCredential(DID, record.connection.id, record.claims),
      res => res && res.thid && res.status !== 500,
      { name: 'issueCredential' }
    )
      .then((res) => {
        debug(CRED_OFFER_SENT)

        return crut.update(record.key, {
          state: CRED_OFFER_SENT,
          offerThid: res.thid
        })
      })
      .catch(console.error) // TODO handle errors!
  }

  else if (record.state === CRED_OFFER_SENT) {
    if (!isRecipient(record, ssb.id)) return

    // NOTE: this was previously getting all offers and accepting them all,
    // this could cause problems
    const offer = await ssb.atalaPrism.awaitMessageResponseForTime(
      record.offerThid,
      'https://didcomm.org/issue-credential/3.0/offer-credential',
      10000
    )

    const result = await ssb.atalaPrism.acceptCredentialOffer(offer)
      .catch(console.error)

    if (!result) return

    const credentials = await ssb.atalaPrism.getAllCredentials()

    // TODO confirm we have the credential we were offered
    if (!credentials.length) {
      console.error('no credentials to respond with currently')
      return
    }

    debug(CRED_COMPLETE)
    crut.update(record.key, { state: CRED_COMPLETE })
      .catch(console.log)
  }

  else {
    if (record.state === CRED_COMPLETE) return
    console.error('Unknown record state:', record.state)
  }
}

async function handlePresentation (ssb, config, crut, record) {
  if (record.state === PRES_CONN_INVITE_SENT) {
    if (!isRecipient(record, ssb.id)) return

    ssb.atalaPrism.acceptConnectionInvitation(record.connection.oob)
      .then(() => {
        debug(PRES_CONN_COMPLETE)
        return crut.update(record.key, { state: PRES_CONN_COMPLETE })
      })
      .catch(console.error) // TODO handle errors!
  }

  else if (record.state === PRES_CONN_COMPLETE) {
    if (!isInitiator(record, ssb.id)) return
    // NOTE : this version means only the welcoming kaitiaki can complete

    const verifierConfig = config.atalaPrism?.verifiers?.[record.tribeId]
    if (!verifierConfig) {
      console.log('No verifier config found for', record.tribeId)
      return
    }

    const verifier = new Verifier(verifierConfig)

    // HACK we have to try multiple times with the verifier sometimes
    // we were getting "invalid connection state" error... so wait a moment?

    retryUntil(
      () => verifier.createPresentationRequest(record.connection.id),
      Boolean,
      { name: 'createPresentationRequest' }
    )
      .then(presentation => {
        debug(PRES_REQUEST_SENT)
        return crut.update(record.key, {
          presentationId: presentation.presentationId,
          state: PRES_REQUEST_SENT
        })
      })
      .catch(err => {
        console.error('failed to create presentation request', { err, record })
        // TODO handle errors!
      })
  }

  else if (record.state === PRES_REQUEST_SENT) {
    if (!isRecipient(record, ssb.id)) return

    // which VC are we going to present?
    // find a credential
    // find presentationRequestMessages
    // accept the presentation
    // mark done

    const credentials = await ssb.atalaPrism.getAllCredentials()
    if (!credentials.length) {
      return
    }
    const credential = credentials.pop()
    // TODO - which credential to present?! How would we handle multiple?

    const messages = await retryUntil(
      () => ssb.atalaPrism.getPresentationRequests(),
      result => result.length > 0,
      { name: 'getPresentationRequests' }
    )

    const results = await Promise.all(
      messages.map(request => {
        return ssb.atalaPrism.acceptPresentationRequest(request, credential)
      })
    ).catch(handleError)

    if (!results) {
      console.log('ARG')
      return
    }

    debug(PRES_SENT)
    crut.update(record.key, { state: PRES_SENT })
      .catch(console.log)
  }

  else if (record.state === PRES_SENT) {
    if (!isInitiator(record, ssb.id)) return

    const { tribeId, presentationId } = record

    const verifierConfig = config.atalaPrism?.verifiers?.[tribeId]
    if (!verifierConfig) {
      console.log('No verifier config found for', tribeId)
      return
    }

    const verifier = new Verifier(verifierConfig)
    const presentationRecord = await retryUntil(
      () => verifier.getPresentation(presentationId),
      (result) => {
        // console.log('presentation.status', result?.status)
        return result?.status === 'PresentationVerified'
      },
      { name: 'getPresentation' }
    )
      .catch(console.error)
    if (!presentationRecord?.data?.[0]) return

    const verification = await verifier.acceptPresentation(presentationId)
      .catch(err => console.error(Error(err, { cause: 'failed to acceptPresentation' })))
    if (!verification) return

    debug(PRES_COMPLETE)
    crut.update(record.key, {
      state: PRES_COMPLETE,
      JWT: presentationRecord.data[0]
    })
      .catch(console.error)
  }

  else {
    if (record.state === PRES_COMPLETE) return
    console.error('Unknown record state:', record.state)
  }
}

function getRoot (m) {
  return (
    m.value.content.tangles.atalaPrism.root ||
    m.key
  )
}

async function retryUntil (fn, isDone = Boolean, opts = {}) {
  const {
    maxRetries = 10,
    delay = 1000,
    name
  } = opts

  let error = new Error('no result found, and no errors D:')
  let result = await fn()
    .catch(err => {
      // swallow the error
      // store a copy of the latest error to throw if needed
      error = err
    })
  let retryCount = 0

  while (!isDone(result) && retryCount < maxRetries) {
    logRetry(name || fn.name, ++retryCount)

    await p(setTimeout)(delay)
    result = await fn()
  }

  if (!isDone(result)) throw new Error(`function ${fn.name} failed to return a result after ${maxRetries} tries`)

  if (result) return result
  else throw error
}

const symbol = '∘'
function logRetry (methodName, count) {
  debug(
    methodName +
    ' ' +
    Array(count).fill(symbol).join('')
  )
}

function isInitiator (record, id) {
  // return record.originalAuthor === id
  return !isRecipient(record, id)
  // NOTE the commented out code means ONLY the kaitiaki who started the process will response
  // saying "not the recipient" means ANY kaitiaki could progress the process :fire:
}
function isRecipient (record, id) {
  return record.recps && record.recps[1] === id
  // assumes recps = [poBoxId, feedId]
}
