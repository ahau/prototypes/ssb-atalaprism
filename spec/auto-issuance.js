const Overwrite = require('@tangle/overwrite')

// states
const CRED_CONN_INVITE_SENT = 'credential connection invite sent'
const CRED_CONN_COMPLETE = 'credential connection complete'
const CRED_OFFER_SENT = 'credential offer sent'
const CRED_COMPLETE = 'credential complete'

const claimsSchema = {
  type: 'object',
  properties: {
    person: {
      type: 'object',
      properties: {
        fullName: { type: 'string' },
        dateOfBirth: { type: 'string' }
      },
      required: ['fullName']
    },
    memberOf: {
      type: 'object',
      properties: {
        tribeId: { type: 'string' },
        tribeName: { type: 'string' }
      },
      required: ['tribeId', 'tribeName']
    },
    required: ['person', 'memberOf']
    // additionalProperties: false
  }
}

// WATCH we may want to seperate out connection + credential tracking records
const spec = {
  type: 'atalaPrism/auto-issuance',
  staticProps: {
    tribeId: { type: 'string' }, // TODO validate more
    claims: claimsSchema,
    connection: {
      type: 'object',
      properties: {
        id: { type: 'string' },
        oob: { type: 'string' }
      },
      required: ['id', 'oob']
    }
  },
  props: {
    state: Overwrite({
      enum: [
        CRED_CONN_INVITE_SENT,
        CRED_CONN_COMPLETE,
        CRED_OFFER_SENT,
        CRED_COMPLETE
      ]
    }),
    offerThid: Overwrite({ type: 'string' })
  }
  // TODO state transitions
}

spec.states = {
  CRED_CONN_INVITE_SENT,
  CRED_CONN_COMPLETE,
  CRED_OFFER_SENT,
  CRED_COMPLETE
}
spec.state = spec.states

module.exports = spec
