# ssb-atala-prism

This module adds **atala-prism-ts-sdk** functionality to **ahau** 

## Setup

- see `.npmrc.template` (TODO add detail)
- `npm i`
- create .env file with the ISSUER_URL and ISSUER_APIKEY and a MEDIATOR DID
- run the tests with `npm test`

## API

### `ssb.atalaPrism.start(cb)`

Starts the AtalaPrism Agent

### `ssb.atalaPrism.offerCredential(tribeId, poBoxId, feedId, claims, cb)`

A method for a kaitiaki to call which kicks off the process of offering a
verifiable credential (VC) to a member of a tribe.

This requires you to have installed `ISSUER_URL` and `ISSUER_APIKEY` for
your tribe (see Config below)

- `claims` is an Object of form
  ```js
  {
    person: {
      fullName: String,
      dateOfBirth: String
    }
  }

  ```
- `cb` callback is of form `cb(err)`
  - If no callback is provided this method returns a promise.


### `ssb.atalaPrism.verifiableCredentials(cb)`

where
- `cb` is of form `cb(err, [VC])`
  - If no callback is provided this method returns a promise.
- `[VC]` is an Array of verifiable credential objects (lots of data!)

### `ssb.atalaPrism.requestPresentation(tribeId, poBoxId, feedId, cb)`

A method for a kaitiaki to kick off a request to present an existing VC to the
tribe. Useful for when someone is applying to a tribe, and they might like to
prove membership of another known tribe.

This requires you to have installed `VERIFIER_URL` and `VERIFIER_APIKEY` for
your tribe (see Config below)

- `cb` callback is of form `cb(err)`
  - If no callback is provided this method returns a promise

### `ssb.atalaPrism.sendCredentialProof(invitationUrl, credentialId, cb)`

A method for a credential holder to make a connection with a verifier and present a credential, where
- `invitationUrl` - an out-of-band (oob) connection invite URL provided by a verifier
- `credentialId` - the id of the credential you'd like to present
- `cb` (optional) - callback function. If not provided, this method returns a Promise


## GraphQL

There are associated graphQL endpoints for each of the above.
See `graphq/typeDefs.js` or `test/graphql` for more details.

## Config

If you're a kaitiaki of a tribe that would like to support Issuing or Verifying
credentials, you'll need to add the following to your ssb config:

JSON:

```json
{
  "atalaPrism": {
    "mediatorDID": "did:peer:2.Ez6LSghwS....",
    "issuers": {
      "[tribeId]": {
        "tribeName": "Whangaroa Papa Hapū",
        "ISSUER_URL": "https://issuer.wph.co.nz/prism-agent",
        "ISSUER_APIKEY": "sdskj087123lekmqawslkdj10p23ulkw1qj3e"
      }
    },
    "verifiers": {
      "[tribeId]": {
        "VERIFIER_URL": "https://verifier.ahau.io/prism-agent",
        "VERIFIER_APIKEY": "k1239asdm1l87sdaj1lkj2387sdjm1lk23m"
      }
    }
  }
}
```

- `mediatorDID` *String* **required** - the `DID` of a mediator responsible for
  relaying messages between the atala prism agents and holders
- `issuers` *Object* - **required** details for each tribeId wanting to issue
  verified credentials
  - `tribeId` *String* - replace with the id for the tribe, **required** for
    every tribe defined. E.g.
    `%mxZ5fD9Prk1b9rl8JIVrQdOiBNUDlngwI3sgkHknPlo=.cloaked`
    - `tribeName` *String* - name of the tribe
    - `ISSUER_URL` *String* - the address in which the issuer atala prism agent
      is running at
    - `ISSUER_APIKEY` *String* - the issuers `API_KEY`

- `verifiers` *Object* - **required** details for each tribeId wanting to verify
  credentials
  - `tribeId` *String* - encryption key for the tribe
    - `VERIFIER_URL`*String* - the address in which the verifier atala prism
      agent is running at
    - `VERIFIER_APIKEY` *String* - the verifiers `API_KEY`

Notes:

- any given tribe can offer issuance and/or verification (no requirement to have
  both)

## Current Issues

- `acceptDIDCommInvitation` doesnt always work, so we have added some logic to
  check for the connection confirmation message, and if confirmation is not
  achieved, than repeat the acceptInvitation process again
- we sometimes see a startup error with the agent

## Development

There is an [AtalaPrism Quick
Start](https://docs.atalaprism.io/docs/quick-start/) guide available where you
can start local instances for each of the following:

- Mediator node
- Issuer node
- Verifier node

Set up the env by copying the template file and filling in the URLs and Keys of
these nodes:
```bash
cp .env.template .env
```

Install dependencies and run the tests:
```bash
npm i
npm test
```

For debug logging:
```bash
DEBUG=ssb-atala* npm test
```

For interactive debugging, run:
```bash
node --inspect-brk test/start.test.js
```
then open `chrome://inspect` in chromium/chrome browser.

See also:
- [`ARCHITECTURE.md`](ARCHITECTURE.md) for an overview of how this module was
designed to work
- [`TODO.md`](TODO.md) to track things that still need to be done

