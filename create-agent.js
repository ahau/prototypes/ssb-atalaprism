const { mkdirSync } = require('fs')
const SDK = require('@atala/prism-wallet-sdk')

const Pluto = require('./pluto')

// === HACK =======================================
const nodeCrypto = require('crypto')

Object.defineProperty(globalThis, 'crypto', {
  value: {
    getRandomValues: (arr) => nodeCrypto.getRandomValues(arr)
  }
})
// ================================================

module.exports = async function createAgent (plutoStorePath, mediatorDIDString) {
  const mediatorDID = SDK.Domain.DID.fromString(mediatorDIDString)

  const apollo = new SDK.Apollo()
  const api = new SDK.ApiImpl()
  const castor = new SDK.Castor(apollo)

  mkdirSync(plutoStorePath, { recursive: true })
  const pluto = await Pluto(plutoStorePath)

  const didcomm = new SDK.DIDCommWrapper(apollo, castor, pluto)
  const mercury = new SDK.Mercury(castor, didcomm, api)
  const store = new SDK.PublicMediatorStore(pluto)
  const handler = new SDK.BasicMediatorHandler(mediatorDID, mercury, store)
  const manager = new SDK.ConnectionsManager(castor, mercury, pluto, handler)

  const seed = apollo.createRandomSeed()
  // TODO persist this?

  return new SDK.Agent(
    apollo,
    castor,
    pluto,
    mercury,
    handler,
    manager,
    seed.seed
  )
}
