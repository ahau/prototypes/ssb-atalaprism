const scuttleTestbot = require('scuttle-testbot')
const ahauServer = require('ahau-graphql-server')
const AhauClient = require('ahau-graphql-client')

require('dotenv').config()

module.exports = async function Testbot (opts = {}) {
  const stack = scuttleTestbot
    .use(require('ssb-backlinks'))
    .use(require('ssb-query'))
    .use(require('ssb-profile'))
    .use(require('ssb-tribes'))
    .use(require('ssb-tribes-registration'))
    .use(require('ssb-blobs'))
    .use(require('../..')) // this plugin

  // scuttlebutt instance
  const ssb = stack({
    db1: true,
    ...opts,
    atalaPrism: {
      mediatorDID: process.env.MEDIATOR_DID,
      ...(opts?.atalaPrism || {})
    }
  })

  const graphqlPort = 3000 + Math.random() * 7000 | 0

  const profile = require('@ssb-graphql/profile')(ssb)
  // graphql API
  const httpServer = await ahauServer({
    schemas: [
      require('@ssb-graphql/main')(ssb),
      profile,
      require('@ssb-graphql/tribes')(ssb, { ...profile.gettersWithCache }),
      require('../../graphql')(ssb)
    ],
    context: {},
    port: graphqlPort
  })
  ssb.close.hook(function (close, args) {
    httpServer.close()
    close.apply(this, args)
  })

  const apollo = new AhauClient(graphqlPort, { isTesting: true, fetch })

  return {
    ssb,
    apollo
  }
}
