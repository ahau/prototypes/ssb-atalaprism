const { test, p, replicate, Testbot, Run, makeBreakpoint, gql } = require('../../helpers')

const pull = require('pull-stream')

const autoIssuanceSpec = require('../../../spec/auto-issuance')
// const Verifier = require('../../../verifier')

const { CRED_COMPLETE } = autoIssuanceSpec.states

// SKIP CI testing as this requires
// - [ ] a real INVITATION_URL from galaxy maps
// - [ ] a webook set up to point to galaxy maps

const INVITATION_URL = ''
// FILL IN TO RUN TEST, otherwise skips

test('galaxy maps GRAPHQL', { skip: !INVITATION_URL.length }, async t => {
  const run = Run(t)

  const rand = Math.round(9999 * Math.random())
  const newMemberName = 'new-member-' + rand
  const kaitiakiName = 'kaitiaiki-' + rand

  const member = await Testbot({ name: newMemberName })

  // <SETUP> kaitiaki (issuer) =============================================== //
  let { ssb: kaitiaki } = await Testbot({ name: kaitiakiName })

  const tribeName = 'Whangaroa Papa Hapu'
  const { groupId: tribeId, poBoxId } = await p(kaitiaki.tribes.create)({ addPOBox: true })

  // NOTE we have to restart the kaitiaki to install the issuer config
  const keys = kaitiaki.keys
  await p(kaitiaki.close)(true)
  const { ssb: kaitiakiAgain } = await Testbot({
    name: kaitiakiName,
    keys,
    rimraf: false,
    atalaPrism: {
      issuers: {
        [tribeId]: {
          tribeName,
          ISSUER_URL: process.env.ISSUER_URL,
          ISSUER_APIKEY: process.env.ISSUER_APIKEY
        }
      }
    }
  })
  kaitiaki = kaitiakiAgain
  // <SETUP> kaitiaki (issuer) =============================================== //

  await run('member agent starts', member.ssb.atalaPrism.start())
  await run('kaitiaki agent starts', kaitiaki.atalaPrism.start())

  replicate({ from: kaitiaki, to: member.ssb, live: true, log: false })
  replicate({ from: member.ssb, to: kaitiaki, live: true, log: false })

  // this breakpoint is something we can "await"
  // it will only by "DONE" when the breakpoint.resolve is called
  const breakpointIssuing = makeBreakpoint()
  pull(
    kaitiaki.messagesByType({
      type: autoIssuanceSpec.type,
      private: true,
      live: true
    }),
    pull.filter(m => !m.sync),
    pull.filter(m => {
      const state = m.value.content?.state?.set
      return state === CRED_COMPLETE
    }),
    pull.drain(breakpointIssuing.resolve)
    // this says "we will be DONE with this breakpoint, when we
    // see our first CRED_COMPLETE state
  )

  const claims = {
    person: {
      fullName: 'Ben Tairea',
      dateOfBirth: '1987/XX/XX'
    }
  }
  await kaitiaki.atalaPrism.offerCredential(
    tribeId,
    poBoxId,
    member.ssb.id,
    claims
  )

  console.log('waiting for auto-issue of VC...')
  await breakpointIssuing

  // DIFFERENCE
  // - oob created on GalaxyMaps site
  // - members starts the process here

  let credential = await member.ssb.atalaPrism.getAllCredentials()
  if (Array.isArray(credential)) credential = credential.pop()

  const result = await run(
    'sendCredentialProof',
    member.apollo.mutate({
      mutation: gql`mutation ($invitationUrl: String!, $credentialId: String!) {
        sendCredentialProof (invitationUrl: $invitationUrl, credentialId: $credentialId)
      }`,
      variables: {
        invitationUrl: INVITATION_URL,
        credentialId: credential.id
      }
    })
  )

  console.log(result)

  await Promise.all([
    p(kaitiaki.close)(true),
    p(member.ssb.close)(true)
  ])

  t.end()
})
