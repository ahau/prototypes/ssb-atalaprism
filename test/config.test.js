const test = require('tape')
const isValid = require('../is-my-config-valid')

const mediatorDID = 'did:peer:2.Ez6LSghwSE437wnDE1pt3X6hVDUQzSjsHzinpX3XFvMjRAm7y.CEYYq6JBVz6Mkhh1e5UcTZ6Cp2ranCWRrv7Yax3Le4N59R6dd.Ly9iZXRhLW1lZGlhdG9yLmF0SeyJ0IjoiZG0iLCJzIjoiaHR0cHM6YWxhcHJpc20uaW8iLCJyIjpbXSwiYSI6WyJkaWRjb21tL3YyIl19'

const tribeId1 = '%zXjl+lh8ZBYBXinYwB4U1MBaQbd6kaJa3byH1BbR1G4=.cloaked'

test('ssb.config.atalaPrism schema', t => {
  t.false(isValid({}), 'empty config')

  // mediatorDID =================================================

  t.true(
    isValid({ mediatorDID }),
    'mediatorDID only'
  )
  t.false(
    isValid({ mediatorDID: 'dog' }),
    'non mediatorDID'
  )
  t.false(
    isValid({ mediatorDID: ' ' + mediatorDID }),
    'malformed mediatorDID'
  )
  t.false(
    isValid({ mediatorDID: mediatorDID + ' ' }),
    'malformed mediatorDID'
  )

  // issuers =====================================================

  t.true(
    isValid({
      mediatorDID,
      issuers: {
        [tribeId1]: {
          tribeName: 'tribio',
          ISSUER_URL: 'https://some-place2.nz',
          ISSUER_APIKEY: 'yess.3\+5AH' // eslint-disable-line
        }
      }
    }),
    'issuer'
  )

  t.false(
    isValid({
      mediatorDID,
      issuers: {
        [tribeId1]: {
          // tribeName: 'tribio',
          ISSUER_URL: 'https://someplace.nz',
          ISSUER_APIKEY: 'yess'
        }
      }
    }),
    'issuer (missing name)'
  )

  t.false(
    isValid({
      mediatorDID,
      issuers: {
        [tribeId1]: {
          tribeName: 'tribio',
          ISSUER_URL: 'http://someplace.nz',
          ISSUER_APIKEY: 'yess'
        }
      }
    }),
    'issuer (no https)'
  )
  t.false(
    isValid({
      mediatorDID,
      issuers: {
        [tribeId1]: {
          tribeName: 'tribio',
          ISSUER_URL: 'https://someplace.nz',
          // ISSUER_APIKEY: 'yess'
          ISSUER_APIKEYY: 'yess'
        }
      }
    }),
    'issuer (missing key)'
  )
  t.false(
    isValid({
      mediatorDID,
      issuers: {
        [tribeId1]: {
          tribeName: 'tribio',
          ISSUER_URL: 'https://someplace.nz',
          ISSUER_APIKEY: 'yess    '
        }
      }
    }),
    'issuer (spaced key)'
  )

  // verifiers ===================================================

  t.true(
    isValid({
      mediatorDID,
      issuers: {
        [tribeId1]: {
          tribeName: 'tribio',
          ISSUER_URL: 'https://some-place2.nz',
          ISSUER_APIKEY: 'yess.3\+5AH' // eslint-disable-line
        }
      },
      verifiers: {
        [tribeId1]: {
          tribeName: 'tribio',
          VERIFIER_URL: 'https://some-place2.nz',
          VERIFIER_APIKEY: 'yess.3\+5AH' // eslint-disable-line
        }
      }
    }),
    'verifier'
  )

  // errorString =================================================

  isValid({
    mediatorDID: 'dog', // <<
    issuers: {
      [tribeId1]: {
        tribeName: 'tribio',
        ISSUER_URL: 'http://some-place2.nz', // <<
        ISSUER_APIKEY: 'sure'
      }
    },
    verifiers: {
    }
  })
  console.log(isValid.errorString)
  t.match(
    isValid.errorString,
    /Invalid.*mediatorDID.*ISSUER_URL/,
    'errorString'
  )

  t.end()
})
