const { test, p } = require('./helpers')
const SDK = require('@atala/prism-wallet-sdk')

// skipped because we don't use this at the moment

test.skip('atalaPrism send message', async t => {
  const apollo = new SDK.Apollo()
  const api = new SDK.ApiImpl()
  const castor = new SDK.Castor(apollo)

  const plutoConfig = {
    type: 'sqljs'
  }

  const pluto = new SDK.Pluto(plutoConfig)

  const mediatorDID = SDK.Domain.DID.fromString(
    'did:peer:2.Ez6LSiekedip45fb5uYRZ9DV1qVvf3rr6GpvTGLhw3nKJ9E7X.Vz6MksZCnX3hQVPP4wWDGe1Dzq5LCk5BnGUnPmq3YCfrPpfuk.SeyJpZCI6Im5ldy1pZCIsInQiOiJkbSIsInMiOiJodHRwczovL21lZGlhdG9yLmpyaWJvLmtpd2kiLCJhIjpbImRpZGNvbW0vdjIiXX0'
  )

  const didcomm = new SDK.DIDCommWrapper(apollo, castor, pluto)
  const mercury = new SDK.Mercury(castor, didcomm, api)
  const store = new SDK.PublicMediatorStore(pluto)
  const handler = new SDK.BasicMediatorHandler(mediatorDID, mercury, store)
  const manager = new SDK.ConnectionsManager(castor, mercury, pluto, handler)
  const seed = apollo.createRandomSeed()
  const agent = new SDK.Agent(
    apollo,
    castor,
    pluto,
    mercury,
    handler,
    manager,
    seed.seed
  )

  agent.addListener('message', (message) => {
    t.pass('Got new message')
    console.log('new message:', message)
  })

  await agent.start()
  t.equal(agent.state, 'running', 'agent is running')

  const secondaryDID = await agent.createNewPeerDID([], true)

  const msg = new SDK.BasicMessage(
    { content: 'Test Message' },
    secondaryDID,
    secondaryDID
  )

  await agent.sendMessage(msg.makeMessage())
    .catch(err => console.log('bad error', err))

  await p(setTimeout)(10000)

  await agent.stop()
  t.end()
})
