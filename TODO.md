# TODO

Now:

- [x] get VC presentations verifying
- [ ] expose getPrismDID in kaitiaki API
- [x] attach the presentation to the auto-presentation record
- [ ] is this presentation trusted?
    - future
      - load the prismDID from the group profile
      - force this to be part of the proof

- [ ] run in mobile
  - [ ] builds
  - [ ] works for a holder (can't easily work for issuers yet because they need
    config)

Later:

- [ ] test persistence
  - can you shut down and start up the agent and recall all the things still?
- [ ] document API
- [x] kaitiaki setup
  - how do we supply ENV in for ISSUER + VERIFIER
- [ ] crash resistence
- [ ] run our own Issuer
  - get these tests passing with this
- [ ] run our own Mediator?

- [ ] auto-accept-credentials (2)
  - [ ] add validations about state-transitions
    - new-member is only allowed to make certain state changes
    - kaitiaki in only allowed to make certain state changes
    - state change path is linear (specific order to follow)
  - [ ] testing
  - [ ] multiple kaitiaki responding?
  - [ ] decide what to do with errors in processing
  - [ ] make config to disable?

- [ ] auto-present-credentials (2)
  - [ ] how to choose which credential is presented
  - [ ] add use of credential proofs
    - [ ] design schemas for membership and whakapapa schemas
    - [ ] add credential schemas
      - where to publish these to?
    - [ ] add trustedIssuers
      - where does this come from?
    - [ ] add reject credential if it fails proofs
  - [ ] make process that takes less steps / requires less online coordination
