const pull = require('pull-stream')
const CRUT = require('ssb-crut')

const autoPresentationSpec = require('../spec/auto-presentation')

module.exports = {
  pullAutoPresentationByRecps
}

function pullAutoPresentationByRecps (ssb, recps) {
  // create a stream of current auto-presentations,
  // specifically ones with the same recps i.e. [poBoxId, feedId]
  const autoPres = new CRUT(ssb, autoPresentationSpec)

  return pull(
    ssb.messagesByType({
      type: 'atalaPrism/auto-presentation',
      private: true
    }),

    pull.filter(m => (
      autoPres.isRoot(m) &&
      recps.every(recp => m.value.content.recps.includes(recp))
    )),

    pull.map(m => m.key),
    pull.unique(),
    pull.asyncMap((id, cb) => autoPres.read(id, cb))
  )
}
