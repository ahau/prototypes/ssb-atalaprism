const { v4: makeUUID } = require('uuid')

module.exports = function Verifier (verifierConfig) {
  const { VERIFIER_APIKEY, VERIFIER_URL } = verifierConfig

  function createConnectionInvitation (opts = {}) {
    return fetch(`${VERIFIER_URL}/connections`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        apiKey: VERIFIER_APIKEY
      },
      body: JSON.stringify({
        label: 'Connect Verifier and Member',
        ...opts
      })
    })
      .then((response) => response.json())
      .then((json) => {
        return json.invitation
      })
  }

  function createPresentationRequest (connectionId, trustedIssuerDID, triesRemaining = 4) {
    return fetch(`${VERIFIER_URL}/present-proof/presentations`, {
      method: 'POST',
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
        apiKey: VERIFIER_APIKEY
      },
      body: JSON.stringify({
        connectionId,
        options: {
          challenge: makeUUID(), // random seed prover has to sign to prevent replay attacks
          domain: 'www.ahau.io'
        },
        proofs: [
          // TODO figure out how to manage proof Schemas
          // https://docs.atalaprism.io/agent-api/#tag/Present-Proof/operation/requestPresentation
          //
          // TODO figure out how / whether to use trustedIssuerDID
        ]
      })
    })
      .then(response => response.json())
      .then(presentation => {
        if (!presentation.presentationId) {
          if (!triesRemaining) throw new Error('unable to createPresentationRequest', { cause: presentation })

          return createPresentationRequest(connectionId, trustedIssuerDID, --triesRemaining)
        }

        return presentation
      })
  }

  function acceptPresentation (presentationId) {
    return fetch(`${VERIFIER_URL}/present-proof/presentations/${presentationId}`, {
      method: 'PATCH',
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
        apiKey: VERIFIER_APIKEY
      },
      body: JSON.stringify({
        action: 'presentation-accept'
        // proofId:
      })
    })
      .then(response => response.json())
  }

  function getPresentation (presentationId) {
    return fetch(`${VERIFIER_URL}/present-proof/presentations/${presentationId}`, {
      method: 'GET',
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/json',
        apiKey: VERIFIER_APIKEY
      }
    })
      .then(response => response.json())
      .then(json => {
        return json
      })
  }

  return {
    createConnectionInvitation,
    createPresentationRequest,
    acceptPresentation,
    getPresentation
  }

  // getPrismDID () {
  //   // get issuers PrismDID
  //   return fetch(`${VERIFIER_URL}/did-registrar/dids`, {
  //     method: 'GET',
  //     headers: {
  //       accept: 'application/json',
  //       'Content-Type': 'application/json',
  //       apiKey: VERIFIER_APIKEY
  //     }
  //   })
  //     .then(response => response.json())
  //     .then(async (json) => {
  //       // NOTE: update to use published DID's
  //       return json.contents[0]?.longFormDid ?? await createPrismDID(VERIFIER_URL, VERIFIER_APIKEY)
  //     })
  // },
}

// function createPrismDID (ISSUER_URL, ISSUER_APIKEY) {
//   console.log('no dids found. Creating one...')
//   return fetch(`${ISSUER_URL}/did-registrar/dids`, {
//     method: 'POST',
//     headers: {
//       accept: 'application/json',
//       'Content-Type': 'application/json',
//       apiKey: ISSUER_APIKEY
//     },
//     body: JSON.stringify({
//       documentTemplate: {
//         publicKeys: [
//           {
//             id: 'auth-1',
//             purpose: 'authentication'
//           },
//           {
//             id: 'assert-1',
//             purpose: 'assertionMethod'
//           }
//         ],
//         services: []
//       }
//     })
//   })
//     .then(response => response.json())
//     .then(json => {
//       return json.longFormDid
//     })
// }
