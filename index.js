const SDK = require('@atala/prism-wallet-sdk')
const CRUT = require('ssb-crut')
const { promisify: p } = require('util')
const c = require('callbackify') // TODO replace with util
const path = require('path')
const debug = require('debug')

const createAgent = require('./create-agent')
const autoRespond = require('./auto-respond')
const Issuer = require('./issuer')
const Verifier = require('./verifier')
const autoIssuanceSpec = require('./spec/auto-issuance')
const autoPresentationSpec = require('./spec/auto-presentation')

const isValidConfig = require('./is-my-config-valid')

const debugConnection = debug('ssb-atala-prism:connection')
const debugCredential = debug('ssb-atala-prism:credential')
const debugSendProof = debug('ssb-atala-prism:sendProof')

const TIMEOUT_FAILURE = 'timeout failure'
const MIN_RUN_TIME = 3000

function handleSendMessageError (err) {
  if (err.message === 'Malformed: Message is not a valid JWE, JWS or JWM') {
    // ignore DIDCommMalformed error. Its a lie
  } else throw err
}

module.exports = {
  name: 'atalaPrism',

  manifest: {
    start: 'async',
    offerCredential: 'async', // graphql
    requestPresentation: 'async', // graphql
    verifiableCredentials: 'async', // graphql
    sendCredentialProof: 'async', // graphql

    acceptConnectionInvitation: 'async',
    getAllPeerDIDs: 'async',
    getAllPrismDIDs: 'async',
    createNewPrismDID: 'async',
    getAllMessages: 'async',
    acceptCredentialOffer: 'async',
    awaitMessages: 'async',
    getPresentationRequests: 'async',
    acceptPresentationRequest: 'async'
  },

  init (ssb, config) {
    let agent
    let agentStartTime
    const crut = {
      issuance: new CRUT(ssb, autoIssuanceSpec),
      presentation: new CRUT(ssb, autoPresentationSpec)
    }

    // shut down atalaPrism agent on ssb close
    ssb.close.hook(async function (close, args) {
      if (agent && agent.stop) {
        // ensure agent has been running for at least MIN_RUN_TIME
        const wait = Math.max(
          0,
          MIN_RUN_TIME - (Date.now() - agentStartTime)
        )
        await p(setTimeout)(wait)
        await agent.stop()
      }
      close.apply(this, args)
    })

    // start edge agent
    async function start (cb) {
      if (cb === undefined) return p(start)()
      if (agent) return cb(null, agent)

      if (!isValidConfig(config.atalaPrism)) {
        const error = new Error(
          isValidConfig.errorString,
          { cause: isValidConfig.errors }
        )
        return cb(error)
      }

      const plutoStorePath = path.join(config.path, 'atalaPrism')
      createAgent(plutoStorePath, config.atalaPrism.mediatorDID)
        .then(_agent => {
          agentStartTime = Date.now()
          agent = _agent
          return agent.start()
        })
        .then(() => {
          autoRespond(ssb, config, crut)

          cb(null, agent)
        })
        .catch(err => {
          // console.log(err)
          cb(err)
        })
    }

    // message listener helper function
    function awaitMessageResponse (thid, piuri) {
      let handleMessage
      const promise = new Promise((resolve, reject) => {
        handleMessage = (messages) => {
          const message = messages.find(m => (
            m.thid === thid &&
            m.piuri === piuri
          ))
          if (!message) return

          agent.removeListener('message', handleMessage)
          resolve(message)
        }

        agent.addListener('message', handleMessage)
      })

      promise.cancel = () => {
        agent.removeListener('message', handleMessage)
      }

      return promise
    }

    // wait 'time' seconds for connection confirmation, else fail
    function awaitMessageResponseForTime (thid, piuri, time) {
      let timer
      const query = awaitMessageResponse(thid, piuri)

      return Promise.race([
        query,
        new Promise((resolve, reject) => {
          timer = setTimeout(resolve, time, TIMEOUT_FAILURE)
        })
        // TODO change this to reject, and then catch and process the error in promise-chain
        // need to check how Promise.race behaves with reject tho!
      ])
        .then(async res => {
          if (res !== TIMEOUT_FAILURE) return res

          // HACK - atala agent / pluto bug
          // sometimes the listener does not work, BUT the message is in pluto (T_T
          const msgs = await ssb.atalaPrism.getAllMessages()
          const msg = msgs.find(m => (
            m.thid === thid &&
            m.piuri === piuri
          ))
          if (!msg) return TIMEOUT_FAILURE
          console.log('FALLBACK - message listener failed, used pluto')
          return msg
        })
        .finally(() => {
          clearTimeout(timer)
          query.cancel()
        })
    }

    // This function
    // - gets the confirmation message (starts a listener which waits 10 seconds)
    // - if that doesn't work, then starts the listener again
    // - if it does this `tries` times, then it fails
    async function acceptConnectionInvitation (invitationURL, triesRemaining = 5, cb) {
      if (cb === undefined) return p(acceptConnectionInvitation)(invitationURL, triesRemaining)
      debugConnection('start: accept connection invitation process (' + triesRemaining + ')')

      debugConnection('  1: process OOB url into an invitation')
      const url = typeof invitationURL === 'string' ? new URL(invitationURL) : invitationURL
      const oobInvitation = await agent.parseOOBInvitation(url)
      const thid = oobInvitation.id

      debugConnection('  2: accept the connection invitation')
      return agent.acceptDIDCommInvitation(oobInvitation)
        .then(() => {
          debugConnection('  3: wait for connection confirmation response from issuer')
          return awaitMessageResponseForTime(
            thid,
            'https://atalaprism.io/mercury/connections/1.0/response',
            10000
          )
        })
        .then(res => {
          if (triesRemaining === 0) throw new Error('Error trying to establish connection')
          if (res === TIMEOUT_FAILURE) {
            triesRemaining--
            // URGENT: @atalaPrism: why does this sometimes fail??
            debugConnection('  4: accepting connection TIMEOUT_FAILURE....i dont know why..., trying again')
            return acceptConnectionInvitation(invitationURL, triesRemaining, cb)
          }
          cb(null, res.id)
        })
        .catch(cb)
    }

    async function acceptCredentialOffer (offerMessage, cb) {
      if (cb === undefined) return p(acceptCredentialOffer)(offerMessage)
      debugCredential('start: accept credential offer process')

      debugCredential('  1: process the "offerMessage" into a "OfferCredential"')
      const offerCredential = await SDK.OfferCredential.fromMessage(offerMessage)

      debugCredential('  2: creates a credential request from the "OfferCredential"')
      agent.prepareRequestCredentialWithIssuer(offerCredential)
        .then(async requestCredential => {
          debugCredential('  3: put the request into a message')
          const message = requestCredential.makeMessage()

          // start listening for a response
          // NOTE we don't await here, because we want to sendMessage after this.
          // we await this query later
          const query = awaitMessageResponseForTime(
            message.thid,
            'https://didcomm.org/issue-credential/3.0/issue-credential',
            60000
          )

          debugCredential('  4: send the message to issuer')
          agent.sendMessage(message).catch(handleSendMessageError)

          debugCredential('  5: wait for the credential to be auto-issued')
          const response = await query
          if (response !== TIMEOUT_FAILURE) return response
          else throw new Error('Failed to get confirmation message')
        })
        .then(credentialMessage => {
          // if (credentialMessage === TIMEOUT_FAILURE) return
          debugCredential('  6: processes the credential message into an "IssuedCredential"')
          const issueCredential = SDK.IssueCredential.fromMessage(credentialMessage)

          debugCredential('  7: processes the IssuedCredential and save it to the DB')
          return agent.processIssuedCredentialMessage(issueCredential)
        })
        .then(res => cb(null, res))
        .catch(cb)
    }

    // TODO callbackify
    function getPresentationRequests (cb) {
      if (cb === undefined) return p(getPresentationRequests)()

      return agent.pluto.getAllMessagesOfType('https://didcomm.atalaprism.io/present-proof/3.0/request-presentation')
        .then(res => cb(null, res))
        .catch(cb)
    }

    // TODO callbackify
    function acceptPresentationRequest (request, credential, cb) {
      if (cb === undefined) return p(acceptPresentationRequest)(request, credential)

      // WIP / HACK / TODO
      if (credential.jti.startsWith('{\"jti"')) { // eslint-disable-line
        console.log('credential.jti still broke yo!')
        credential.jti = JSON.parse(credential.jti).jti
      }

      return agent.createPresentationForRequestProof(request, credential)
        .then(presentation => {
          const message = presentation.makeMessage()

          // const response = getConfirmationMessage('https://didcomm.org/present-proof/3.0/request-presentation', 20000)

          return agent.sendMessage(message).catch(handleSendMessageError)
        })
        .then(res => cb(null, res))
        .catch(cb)
    }

    // TODO callbackify
    function offerCredential (tribeId, poBoxId, feedId, claims, cb) {
      if (cb === undefined) return p(offerCredential)(tribeId, poBoxId, feedId, claims)

      if (claims.tribe) return cb(Error('cannot inject claims.tribe'))

      const issuerConfig = config.atalaPrism?.issuers?.[tribeId]
      if (!issuerConfig) return cb(Error('No issuer config found for ' + tribeId))
      const issuer = new Issuer(issuerConfig)

      issuer.createConnectionInvitation()
        .then(invitation => {
          return crut.issuance.create({
            tribeId,
            claims: {
              memberOf: {
                tribeId,
                tribeName: issuerConfig.tribeName
              },
              ...claims
            },
            connection: {
              id: invitation.id,
              oob: invitation.invitationUrl
            },
            state: autoIssuanceSpec.states.CRED_CONN_INVITE_SENT,
            recps: [poBoxId, feedId]
          })
        })
        .then(msgId => cb(null, msgId))
        .catch(cb)
    }
    // WIP
    // - [ ] find difference between
    //    - getAllCredentials
    //    - verifiableCredentials

    function requestPresentation (tribeId, poBoxId, feedId, cb) {
      // this kicks of the auto-respond chain for presentation
      // could consider renaming "start presentation process"
      if (cb === undefined) return p(requestPresentation)(tribeId, poBoxId, feedId)

      const verifierConfig = config.atalaPrism?.verifiers?.[tribeId]
      if (!verifierConfig) return cb(Error('No verifier config found for ' + tribeId))
      const verifier = new Verifier(verifierConfig)

      verifier.createConnectionInvitation()
        .then(invitation => {
          return crut.presentation.create({
            tribeId,
            connection: {
              id: invitation.id,
              oob: invitation.invitationUrl
            },
            state: autoPresentationSpec.state.PRES_CONN_INVITE_SENT,
            recps: [poBoxId, feedId]
          })
        })
        .then(msgId => cb(null, msgId))
        .catch(cb)
    }

    function sendCredentialProof (invitationUrl, credentialId, cb) {
      if (cb === undefined) return p(sendCredentialProof)(invitationUrl, credentialId)
      debugSendProof('start: send credential proof')

      const invite = extractInviteFromInvitationUrl(invitationUrl)
      if (!invite) return cb(new Error('invalid invitiationUrl', { cause: invitationUrl }))

      // wait for request from Verifier
      agent.addListener('message', handlePresentationRequest)

      async function handlePresentationRequest (messages) {
        const message = messages.find(message => (
          message.piuri === 'https://didcomm.atalaprism.io/present-proof/3.0/request-presentation' &&
          message.from.toString() === invite.from
        ))
        if (!message) return

        debugSendProof('messageListner: found presentation message from invite')
        // get credential
        const credential = await agent.pluto.getAllCredentials()
          .then(credentials => {
            if (!Array.isArray(credentials)) return credentials
            return credentials.find(cred => cred.id === credentialId)
          })

        if (!credential) return cb(new Error('Error finding credential'))

        try {
          debugSendProof('messageListner: got credential from credential.id')
          // accept presentation request
          await acceptPresentationRequest(message, credential)

          cb(null, 'request accepted')
        } catch (err) {
          cb(err)
        }

        agent.removeListener('message', handlePresentationRequest)
      }

      // kick off the connection accept
      // => verifier accepts connection, then issues presentation request
      // (webhook: verifier pushes update to server, which can then progress
      // to sending presentation request)
      acceptConnectionInvitation(invitationUrl)
        .catch(err => cb(new Error(err, { cause: 'sendCredentialProof failed at acceptConnectionInvitation' })))
    }

    return {
      start,
      offerCredential,
      requestPresentation,
      sendCredentialProof,
      verifiableCredentials: c(() => agent.verifiableCredentials()),

      // lower level methods
      acceptConnectionInvitation,
      getAllPeerDIDs: c(() => agent.pluto.getAllPeerDIDs()),
      getAllPrismDIDs: c(() => agent.pluto.getAllPrismDIDs()),
      createNewPrismDID: c((alias, service) => agent.createNewPrismDID(alias, service)),
      acceptCredentialOffer,
      getAllCredentials: c(() => agent.pluto.getAllCredentials()),
      getAllMessages: c(() => agent.pluto.getAllMessages()),
      awaitMessages: c(() => agent.manager.awaitMessages()),
      getPresentationRequests,
      acceptPresentationRequest,

      // for testing only
      addListener: (type, cb) => agent.addListener(type, cb),
      awaitMessageResponseForTime
    }
    // c = callbackify
    // Note
    //   - callbackify turns a promise into a callback
    //   - if you don't pass a callback, it falls back to Promise mode!
    //   - we don't have agent straight off, so we have to write it ugly...
  }
}

function extractInviteFromInvitationUrl (oobInvitationUrl) {
  const code = oobInvitationUrl.split('oob=')[1]
  if (!code) return

  try {
    return JSON.parse(Buffer.from(code, 'base64').toString())
  } catch (err) {
    console.log(err)
  }
}
