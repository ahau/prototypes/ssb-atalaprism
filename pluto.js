module.exports = async function pluto (plutoStorePath) {
  const { createLevelDBStorage } = require('@pluto-encrypted/leveldb')
  const { Database } = require('@pluto-encrypted/database')
  const { getDefaultCollections } = require('@pluto-encrypted/schemas')

  const defaultPassword = new Uint8Array(32).fill(1)
  return Database.createEncrypted(
    {
      name: 'my-db', // TODO change
      encryptionKey: defaultPassword,
      storage: createLevelDBStorage({
        dbName: 'ssb-atala-prism',
        dbPath: plutoStorePath
      }),
      collections: getDefaultCollections()
    }
  )
}
