# Architecture

This is a `secret-stack` plugin, which allows it to be integrated into, and
access our scuttlebutt stack.

This module has 3 main areas:
1. low-level APIs for performing discrete actions
2. high-level automation around VC issuance/presentation
3. graphql schema to expose relevent queries/mutations in front-end

This module is designed to be used by community members ("Holders" of VCs),
kaitaiki ("Issuers" AND "Verifiers" of VCs). In our context any peer could
simultaneously be all roles at once (though usually for different groups).

## 1. Low-level APIs

see `index.js`

`ssb.atalaPrism.start()` takes care of initializing an atala-prism agent using
the SDK. This is primarily for Holders.


## 2. Hih-level automation

One of the challenges in AtalaPrism is how out-of-band communication is
achieved. In our context peers are already in communication via p2p replication
(over scuttlebutt), so we use this as a messaging layer for many steps.

### Auto-issuance of Verifiable Credentials

NOTES
- Roles
  - **Peer** - a Scuttlebutt peer who is applying to join a group the Kaitiaki is administering
  - **Kaitiaki** - the custodian of a group, they are also in charge of issuing VCs.
  - **Issuer** an Open Enterprise Agent offering an API
  - **Mediator** (not shown), facillitates delay tolerant communication
- All communication between the Peer + Kaitiaki is via Scuttlebutt private messaging
- All communication between the Kaitiaki + Issuer is by http(s)
- All communication between Peer + Issuer is via DIDComm and handled by AtalaPrism

```mermaid
sequenceDiagram
  autonumber
  
  actor Peer
  actor Kaitiaki
  participant Issuer
  
  Peer->>  Kaitiaki: registration

  Kaitiaki ->> Peer: add-member
  
  rect rgb(191, 223, 255)
    note right of Peer: Establish Connection 

    Kaitiaki ->> +Issuer: make connection inivte (http)
    Issuer -->> -Kaitiaki: oob invite

    Kaitiaki ->> Peer: CONN_INVITE (+oob)


    Peer ->> Issuer: agent.acceptDIDCommInvitation
    Peer ->> Kaitiaki: CONN_COMPLETE
  end
  
  rect rgb(223, 191, 255)
    note right of Peer: Issue Credential
    
    Kaitiaki ->> +Issuer: make cred offer (http)
    Issuer -->> -Kaitiaki: OK

    Kaitiaki ->> Peer: OFFER_SENT
    
    Issuer ->> Peer: Credential Offer
    Peer ->> Issuer: Accept Credential
    Issuer ->> Peer: Verifiable Credential
    Peer ->> Kaitiaki: COMPLETE
  end
```

1. The Peer registers to join a tribal group
2. The Kaitiaki accepts the registration, and sends an "add-member" message, which gives the new member the group secret (and announces to existing members that there is a new member)
3. The Kaitiaki starts auto-issuing a VC by asking for a connection invite from the Issuer. This will be used to forge a DIDComm connection with the Peer
4. The Issuer sends back and Out-of-band invite (for the Peer to use)
5. The Kaitiki sends the oob invite to the Peer
6. The Peer uses the oob invite. A connection now exists which allows DIDComm messaging between Issuer and Peer
7. The Peer messages the Kaitiaki saying "connection is complete"
8. The Kaitiaki progresses to making a Credential Offer. They do this by commanding the Issuer node to issue an offer
9. The receive an OK from the Issuer
10. They tell the Peer that an Offer is on the way
11. The Issuer sends an offer, which eventually gets to the Peer
12. The Peer (who has been listening for the Offer) gets the offer, and accepts it
13. Issuer sends Verifiable Credential
14. Peer marks the process COMPLETE

--- 

Information about the current state of coordination between the Peer and Kaitiaki is shared in a collaborative (private) record. The shared state progresses:
- `CONN_INVITE` - signals to the peer that a connection invite has been issued (and here are the details)
- `CONN_COMPLETE` - signals to the kaitiaki that the connection invite was accepted, and we're ready to proceed to issuance over that connection
- `OFFER_SENT` - signals that a credential offer has been issued, and you can expect it to arrive soon
- `COMPLETE` - signals that the whole process is complete, and that the verifiable credential has been received by the peer.

### Auto-presentation of Credentials

```mermaid
sequenceDiagram
  autonumber
  
  actor Peer
  actor Kaitiaki
  participant Verifier
  
  Peer->>  Kaitiaki: registration

  rect rgb(191, 223, 255)
    note right of Peer: Establish Connection 

    Kaitiaki ->> +Verifier: make connection inivte (http)
    Verifier -->> -Kaitiaki: oob invite

    Kaitiaki ->> Peer: CONN_INVITE (+oob)


    Peer ->> Verifier: agent.acceptDIDCommInvitation
    Peer ->> Kaitiaki: CONN_COMPLETE
  end
  
  rect rgb(223, 191, 255)
    note right of Peer: Present Credential
    
    Kaitiaki ->> +Verifier: make presentation req. (http)
    Verifier -->> -Kaitiaki: OK

    Kaitiaki ->> Peer: REQUEST_SENT (+presentationId)
    
    Verifier ->> Peer: Presentation Request
    Peer ->> Verifier: Presentation
    Peer ->> Kaitiaki: PRES_SENT
    Kaitiaki ->> +Verifier: get presentation (http)
    Verifier -->> -Kaitiaki: Verified
    Kaitiaki ->> Peer: COMPLETE
  end
  
  Kaitiaki ->> Peer: add-member
```

TODO: describe all steps!
