module.exports = function Issuer (issuerConfig) {
  const { ISSUER_URL, ISSUER_APIKEY } = issuerConfig

  return {
    createConnectionInvitation () {
      return fetch(`${ISSUER_URL}/connections`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          apiKey: ISSUER_APIKEY
        },
        body: JSON.stringify({
          label: 'Connect Issuer and Member'
        })
      })
        .then((response) => response.json())
        .then((json) => {
          return json.invitation
        })
    },

    getPrismDID () {
      // get issuers PrismDID
      return fetch(`${ISSUER_URL}/did-registrar/dids`, {
        method: 'GET',
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
          apiKey: ISSUER_APIKEY
        }
      })
        .then(response => response.json())
        .then(async (json) => {
          // NOTE: update to use published DID's
          return json.contents[0]?.longFormDid ?? await createPrismDID(ISSUER_URL, ISSUER_APIKEY)
        })
    },

    issueCredential (didPrism, connectionId, claims) {
      return fetch(`${ISSUER_URL}/issue-credentials/credential-offers`, {
        method: 'POST',
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
          apiKey: ISSUER_APIKEY
        },
        body: JSON.stringify({
          claims,
          issuingDID: didPrism,
          connectionId,
          automaticIssuance: true
        })
      })
        .then(response => response.json())
        .then(json => {
          return json
        })
    }
  }
}

function createPrismDID (ISSUER_URL, ISSUER_APIKEY) {
  console.log('no dids found. Creating one...')
  return fetch(`${ISSUER_URL}/did-registrar/dids`, {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'Content-Type': 'application/json',
      apiKey: ISSUER_APIKEY
    },
    body: JSON.stringify({
      documentTemplate: {
        publicKeys: [
          {
            id: 'auth-1',
            purpose: 'authentication'
          },
          {
            id: 'assert-1',
            purpose: 'assertionMethod'
          }
        ],
        services: []
      }
    })
  })
    .then(response => response.json())
    .then(json => {
      return json.longFormDid
    })
}
