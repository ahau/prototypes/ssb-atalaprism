const Validator = require('is-my-json-valid')
const schema = require('./spec/config')

const isValid = Validator(schema, { verbose: true })
Object.defineProperty(isValid, 'errorString', {
  get () {
    if (!this.errors) return ''

    const lines = this.errors
      .map(err => {
        const path = err.field
          .replace(/^data/, 'atalaPrism')
          .split('.')

        return path
          .map((el, i) => i < path.length - 1 ? chill(el) : red(el))
          .join('.')
      })
      .join(', ')

    return `Invalid config: ${lines}`
  }
})

module.exports = isValid

function red (string) {
  return `\x1b[31m\x1b[1m${string}\x1b[0m`
}
function chill (string) {
  return `\x1b[36m${string}\x1b[0m`
}
